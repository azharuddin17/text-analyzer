import logo from './logo.svg';
import './App.css';
import Navbar from './components/Navbar';
import TextArea from './components/TextArea';
import React, { useState } from 'react'
import Alert from './components/Alert';
import About from './components/About';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Routes
} from "react-router-dom";





function App() {

  const[mode,setMode]=useState('light')

  const[alert,setAlert]=useState(null)

  const showAlert=(message,type)=>{

    setAlert({
      msg:message,
      type:type
    })
  }

  setTimeout(() => {
    
    setAlert(null)
   
  }, 2000);

  const togglemode=()=>{
    if(mode === 'dark'){
      setMode('light')
      document.body.style.backgroundColor='white';
      showAlert("Light mode enabled","success")
      document.title='TEXTUTILS-LIGHT MODE'

      setInterval(() => {
        
        document.title='TEXTUTILS-HOME'
      }, 2000);
      
    }
    else{
      setMode('dark')
      document.body.style.backgroundColor='black';
      showAlert("Dark mode enabled","success")
      document.title='TEXTUTILS-DARK MODE'
    }
  }
  return (
<>
<Router>
<Navbar title="AppUtils"  mode={mode} togglemode={togglemode}/>
<Alert alert={alert}/>

<div className="container my-3">



<Switch>

          <Route exact path="/about">
            <About />
          </Route>

          <Route exact path="/">
          <TextArea heading="TEXT ANALYSER" showAlert={showAlert} mode={mode} />
          </Route>
          
        </Switch>
        
</div>
</Router>


    </>
  );
}


export default App;
