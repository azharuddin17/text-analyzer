import React, {useState} from 'react'
import PropTypes from 'prop-types'


export default function TextArea(props) {

    const HandleOnChange= (event)=>{
        SetText(event.target.value)
        console.log("Text has been changed");
    }

    const HandleOnClick=()=>{
        console.log("Button has been clicked");
        let newText=text.toUpperCase();
        SetText(newText);
        props.showAlert("Text has been changed to Upper Case", "success");
        
    }

    const HandleOnLowClick=()=>{
      console.log("Button has been clicked");
      let newText=text.toLowerCase();
      SetText(newText);
      props.showAlert("Text has been changed to Lower Case", "success");
      
      
  }

  const HandleOnClear=()=>{
    console.log("Button has been clicked");
    let newText='';
    SetText(newText);
    props.showAlert("Text has been cleared", "success");
    
}

  // const handleMode=()=>{
  //   if(myStyle.color==='black'){
  //     setMyStyle({
  //       color:'white',
  //       backgroundColor:'black'
  //     })
  //     setbtnText("Enable Light Mode")
  //   }
  //   else{
  //    setMyStyle({
  //     color:'black',
  //       backgroundColor:'white'
  //    })
  //    setbtnText("Enable Dark Mode")
  //   }
  // }

  //COPY TO CLIPBOARD
  const HandleOnCopy=()=>{
    console.log("Text has been copied")
    var ele=document.getElementById("comments");
    ele.select();
    
    navigator.clipboard.writeText(ele.value);
    props.showAlert("Text has been copied", "success");

  }

  //REMOVE EXTRA SPACES
  const HandleExtraSpaces=()=>{
    let newText=text.split(/[ ]+/)
    SetText(newText.join(" "))
    props.showAlert("Extra spaces has been removed", "success");
  }


    const[text,SetText]=useState("");

    // const[myStyle,setMyStyle]=useState({
    //   color:'black',
    //   backgroundColor:'white'

    // })

    // const[btnText,setbtnText]=useState("Enable Dark Mode")


  return (
    <>
   
    <div>
 <div className="container" style={{color: props.mode === 'dark'?'white':'black'}} >
    <h1>{props.heading}</h1>
  
  <textarea className="form-control" value={text} onChange={HandleOnChange}  id="comments" style={{backgroundColor: props.mode === 'dark'?'black':'white', color : props.mode === 'dark'?'white':'black'}}  rows="3" ></textarea>
  <button className="btn btn-primary my-3 mx-1" onClick={HandleOnClick} >Click to convert to Upper Case</button>
  <button className="btn btn-primary my-3 mx-1" onClick={HandleOnLowClick} >Click to convert to Lower Case</button>
  <button className="btn btn-primary my-3 mx-1" onClick={HandleOnClear} >Clear Text</button>
  <button className="btn btn-primary my-3 mx-1" onClick={HandleOnCopy} >Copy to Clipboard</button>
  <button className="btn btn-primary my-3 mx-1" onClick={HandleExtraSpaces} >Remove Extra Spaces</button>
   
</div>

<div className="container my-3 " style={{color: props.mode === 'dark'?'white':'black'}} >
  <h3>TEXT SUMMARY</h3>
  <p>No.of words are:{text.split(/\s+/).filter((Element)=>{return Element.length!==0}).length} and No. of letters are: {text.length}</p>
  <p>Time taken to read the text is {0.008 *text.split(" ").length} minutes </p>
  <h5>PREVIEW</h5>
  <p>{text.length>0?text:"Please enter anything in text box to preview it"}</p>
</div>

{/* <div className="modes" >
<button className="btn btn-dark my-2 mx-1 " onClick={handleMode} >{btnText}</button>

</div> */}
    </div>
    
    </>
  )
}
