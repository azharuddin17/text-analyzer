import React, {useState} from 'react'
import PropTypes from 'prop-types'


export default function TextArea(props) {

    const HandleOnChange= (event)=>{
        SetText(event.target.value)
        console.log("Text has been changed");
    }

    const HandleOnClick=()=>{
        console.log("Button has been clicked");
        let newText=text.toUpperCase();
        SetText(newText);
        
    }

    const HandleOnLowClick=()=>{
      console.log("Button has been clicked");
      let newText=text.toLowerCase();
      SetText(newText);
      
  }

  const HandleOnClear=()=>{
    console.log("Button has been clicked");
    let newText='';
    SetText(newText);
    
}

  const handleMode=()=>{
    if(myStyle.color==='black'){
      setMyStyle({
        color:'white',
        backgroundColor:'black'
      })
      setbtnText("Enable Light Mode")
    }
    else{
     setMyStyle({
      color:'black',
        backgroundColor:'white'
     })
     setbtnText("Enable Dark Mode")
    }
  }

  //COPY TO CLIPBOARD
  const HandleOnCopy=()=>{
    console.log("Text has been copied")
    var ele=document.getElementById("comments");
    ele.select();
    
    navigator.clipboard.writeText(ele.value);

  }

  //REMOVE EXTRA SPACES
  const HandleExtraSpaces=()=>{
    let newText=text.split(/[ ]+/)
    SetText(newText.join(" "))
  }


    const[text,SetText]=useState("Please enter the text");

    const[myStyle,setMyStyle]=useState({
      color:'black',
      backgroundColor:'white'

    })

    const[btnText,setbtnText]=useState("Enable Dark Mode")


  return (
    <>
   
    <div>
 <div className="container" style={myStyle}>
    <h1>{props.heading}</h1>
  
  <textarea className="form-control" value={text} onChange={HandleOnChange}  id="comments" rows="3" ></textarea>
  <button className="btn btn-primary my-3 mx-1" onClick={HandleOnClick} >Click to convert to Upper Case</button>
  <button className="btn btn-primary my-3 mx-1" onClick={HandleOnLowClick} >Click to convert to Lower Case</button>
  <button className="btn btn-primary my-3 mx-1" onClick={HandleOnClear} >Clear Text</button>
  <button className="btn btn-primary my-3 mx-1" onClick={HandleOnCopy} >Copy to Clipboard</button>
  <button className="btn btn-primary my-3 mx-1" onClick={HandleExtraSpaces} >Remove Extra Spaces</button>
   
</div>

<div className="container my-3 " >
  <h3>TEXT SUMMARY</h3>
  <p>No.of words are:{text.split(" ").length} and No. of letters are: {text.length}</p>
  <p>Time taken to read the text is {0.008 *text.split(" ").length} minutes </p>
  <h5>PREVIEW</h5>
  <p>{text}</p>
</div>

<div className="modes" >
<button className="btn btn-dark my-2 mx-1 " onClick={handleMode} >{btnText}</button>

</div>
    </div>
    
    </>
  )
}
